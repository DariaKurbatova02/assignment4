package com.example;
import java.lang.Math;
//Daria Kurbatova
//2043755
/**
 * Cylinder is a class used to create a cylinder
 * @author Daria Kurbatova
 * @version 12/09/2022
 */
public class Cylinder {

    private double height;
    private double radius;

    public Cylinder (double height, double radius){
        this.height = height;
        this.radius = radius;
    }
    public double getHeight(){
        return this.height;
    }
    public double getRadius(){
        return this.radius;
    }
    /**
     * calculates the volume of this cylinder
     * @return volume of this cylinder
     */
    public double getVolume(){
        return (Math.PI * Math.pow(this.radius, 2) * this.height);
    }
    /**
     * calculates the surface area of this cylinder
     * return surface area of this cylinder
     */
    public double getSurfaceArea(){
        return ((2 * (Math.PI) * Math.pow(this.radius, 2)) + (2 * Math.PI * this.radius * this.height));
    }

}
