package com.example;
//Polina Yankovich 1834306
public class Cone {
    private double height; 
    private double radius; 

    public Cone(double height, double radius){
        this.height=height;
        this.radius=radius;
    }

    public double getHeight(){
       return this.height;
       //return 0;
    }
    public double getRadius(){
        return this.radius;
        //return 0;
    }

    public double getVolume() {
        return (Math.PI * Math.pow(this.radius, 2) * (this.height/3));
        //return 0;
    }
    public double getSurfaceArea() {
        return (Math.PI*this.radius*(this.radius+(Math.sqrt(Math.pow(this.height,2)+Math.pow(this.radius,2)))));
        //return 0;
    }
}
