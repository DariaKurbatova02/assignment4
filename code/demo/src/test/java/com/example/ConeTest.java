package com.example;
//Daria Kurbatova
//2043755
import static org.junit.Assert.*;
import org.junit.Test;
import java.lang.Math;
public class ConeTest {
    @Test
    // test get method for height of cone
    public void testGetMethodForHeight(){
        Cone c = new Cone(15, 3);
        assertEquals(15, c.getHeight(), 0);
    }
    @Test
    // test get method for radius of cone
    public void testGetMethodForRadius(){
        Cone c = new Cone(17, 11);
        assertEquals(11, c.getRadius(), 0);
    }
    @Test
    // test get method for surface area of cone
    public void testGetSurfaceArea(){
        Cone c = new Cone(3, 7);
        double expectedSurfaceArea = Math.PI * 7 * (7 + Math.sqrt((Math.pow(3,2) + (Math.pow(7,2)))));
        assertEquals(expectedSurfaceArea, c.getSurfaceArea(), 0);
    }
    @Test
    // test the method for volume calculation
    public void testGetVolume(){
        Cone c = new Cone(5, 7);
        double expectedVolume = Math.PI * (7*7) * (5.0/3);
        assertEquals(expectedVolume, c.getVolume(), 0);
    }

}
