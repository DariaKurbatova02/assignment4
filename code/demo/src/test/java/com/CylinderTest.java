//Polina Yankovich ID 1834306 12-09-2022
package com;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import com.example.Cylinder;

public class CylinderTest {
   @Test
    public void testGetHeight(){
        Cylinder testCylinder = new Cylinder(5,2);
        double expectedValue=5;
        assertEquals(expectedValue,testCylinder.getHeight(),0);
    }
    @Test
    public void testGetRadius(){
        Cylinder testCylinder = new Cylinder(2,7);
        double expectedValue=7;
        assertEquals(expectedValue,testCylinder.getRadius(),0);
    }
    @Test 
    public void testGetVolume(){
        Cylinder testCylinder = new Cylinder(2,7);
        double expectedValue= (Math.PI * Math.pow(7, 2) * 2);
        assertEquals(expectedValue,testCylinder.getVolume(),0);

    }
    @Test 
    public void testGetArea(){
        Cylinder testCylinder = new Cylinder(2,7);
        double expectedValue=((2 * (Math.PI) * Math.pow(7, 2)) + (2 * Math.PI * 7 * 2));
        assertEquals(expectedValue,testCylinder.getSurfaceArea(),0);

    }

}
